#include <stdio.h>

int main(void){
    printf("%d", isPrime(13,7));
    printf("%d", gcd(32,15));
    printf("%d", nrDigits(1234562));
}
int isPrime(int number, int halfOfTheNumber){

    if(halfOfTheNumber == 1){
        return 1;
    }
    else
    {
       if(number % halfOfTheNumber == 0)
         return 0;
       else
         isPrime(number, halfOfTheNumber - 1);
    }
}

int gcd(int firstNumber, int secondNumber) {
    
    if (secondNumber != 0)
    {
        return gcd(secondNumber, firstNumber % secondNumber);
    }
    else
    {
        return firstNumber;
    }
}
int nrDigits(int number){
    
    static int count = 0;
    if (number > 0)
    {
        count++;
        nrDigits(number / 10);
    }
    else 
    {
        return count;
    }
}
