#include <studio.h>
FILE *file ;

int arb_func(float x)
    return x*log10(x)-1.2

void rf (float *x, float x0, float x1, float fx0 , float fx1, int *itr) {
    *x = ((x0 * fx1) - (x1 * fx0)) / (fx1 - fx0);
    *itr=*itr+1; //indirection means pointer value.
    printf("iteration %d: %0.5f\n", *itr, *x);
    fprintf(file, "iteration %d : %0.5f\n", *itr, *x);
}

int main ()
    file = fopen("rf.txt", "w");
    int itr, maxitr ;
    float x0 ,x1,x_curr,x_next,error ;
    printf("Please enter interval values x0,x1,error and iterations numbers :");
    scanf("%f %f %f %d", &x0, &x1, &error, &maxitr);

    rf(&x_curr, x0, x1, arb_func(x0), arb_func(x1), &itr);

    // do while 
    do
    {

        if (arb_func(x0) * arb_func(x_curr) < 0)
        {
            x1 = x_curr;
        }
        else
        {
            x0 = x_curr;
        }

        rf(&x_next, x0, x1, arb_func(x0), arb_func(x1), &itr);
        if (fabs(x_next - x_curr) < error)
        {
            printf("After %d iterations,root is %0.5f ", itr, x_curr);
            fprintf(file, "After %d iterations, root is %0.5f\n", itr, x_curr);
            return 0;
        }
        else
        {
            x_curr=x_next;
        }
    } while (itr < maxitr);
    printf("Solution does not converge or iteration not sufficient: ");
    fclose(file);
    return 1;
}
 
